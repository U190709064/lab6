public class Circle {
    int radius;
    Point center;

    public Circle(int radius, int x, int y ) {
        this.radius = radius;
        this.center = new Point(x, y);
    }

    public double area() {
        return Math.PI * this.radius * this.radius;
    }

    public double perimeter() {
        return 2 * Math.PI * this.radius;
    }

    public static boolean intersect(Circle c1, Circle c2) {
        double distanceBetweenCenters = Math.sqrt(
                Math.pow(c1.center.xCoord - c2.center.xCoord , 2) +
                        Math.pow(c1.center.yCoord - c2.center.yCoord , 2));
        return !((c1.radius + c2.radius) < distanceBetweenCenters) &&
                !(Math.abs(c1.radius - c2.radius) > distanceBetweenCenters);
    }
}