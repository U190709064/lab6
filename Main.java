public static void main(String[] args) {
        Rectangle r = new Rectangle(3,4,8,3);


        System.out.println("Area of rectangle: " + r.area() +
                "\nPerimeter of rectangle: " + r.perimeter() );

        System.out.println("Corners of rectangle: ");
        r.corners();

        Circle circle1 = new Circle(10,5,5);
        System.out.println("Area of circle: " + circle1.area() +
                "\nPerimeter of circle: " + circle1.perimeter() );

        Circle circle2 = new Circle(5,3,8);

        if (Circle.intersect(circle1,circle2))
            System.out.println("Circles are intersect.");
        else
            System.out.println("Circles are not intersect.");
    }
}