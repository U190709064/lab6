public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, int x, int y) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = new Point(x, y);
    }

    public int area() {
        return this.sideA * this.sideB;
    }

    public int perimeter() {
        return 2 * (this.sideA + this.sideB);
    }

    public void corners() {
        String topL3ft = topLeft.xCoord + "," + topLeft.yCoord;
        String topRight = (topLeft.xCoord + sideA) + "," + topLeft.yCoord;
        String bottomLeft = topLeft.xCoord + "," + (topLeft.yCoord - sideB);
        String bottomRight = (topLeft.xCoord + sideA) + "," + (topLeft.yCoord - sideB);
        System.out.println("Top Left:" + topL3ft + "\nTop Right: " + topRight +
                "\nBottom Left: "+ bottomLeft + "\nBottom Right: "+ bottomRight);
    }
}